import os, shutil

if os.sys.platform == "linux":
	home = os.environ["HOME"]
	desktop = "Desktop"

	if not os.path.exists(os.path.join(home, desktop)):
		desktop = "Masaüstü"

	pwd = os.getcwd()

	openbox_path = os.path.join(home, ".config", "openbox")

	if not os.path.exists(openbox_path):
		os.mkdir(openbox_path)

	old_shortcut = os.path.join(home, desktop, "client_shortcut_linux.desktop")

	if os.path.exists(old_shortcut):
		os.remove(old_shortcut)

	old_config = os.path.join(home, ".ogr_info")

	if os.path.exists(old_config):
		shutil.rmtree(old_config)

	shortcut_name = "client_linux_shortcut.desktop"
	shortcut_icon = os.path.join(pwd, "assets", "logo.png")

	shorcut_path = os.path.join(home, desktop, shortcut_name)

	shortcut_text = "[Desktop Entry]\nType=Application\nName=HARUZEM EXAM CLIENT\nExec=/usr/bin/python3 {}/start_exam_client.pyw\nIcon={}".format(pwd, shortcut_icon)

	shortcut_file = open(shorcut_path, "w")
	shortcut_file.write(shortcut_text)
	shortcut_file.close()

	service_path = os.path.join('misc', "auto_pull.service")

	service_text = "[Unit]\nDescription=Auto Pull Service\nRequires=network-online.target\nAfter=network-online.target systemd-resolved.service\n\n[Service]\nType=idle\nUser=pi\nWorkingDirectory={}\nExecStart=/bin/bash {}/misc/auto_pull\n\n[Install]\nWantedBy=multi-user.target".format(pwd, pwd)

	service_file = open(service_path, "w")
	service_file.write(service_text)
	service_file.close()


	# autostart_path = os.path.join(home, ".config", "autostart")

	# if not os.path.exists(autostart_path):
	# 	os.mkdir(autostart_path)

	# autostart_path = os.path.join(autostart_path, shortcut_name)
	
	# autostart_shortcut = open(autostart_path, "w")
	# autostart_shortcut.write(shortcut_text)
	# autostart_shortcut.close()
