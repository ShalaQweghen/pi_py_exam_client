import socket

from lib.connection import Connection

class Client(Connection):
  def __init__(self, ip, port):
    Connection.__init__(self)

    self.ip = ip
    self.port = port

    self.conn.connect((self.ip, self.port))

  def reconnect(self):
    while True:
      self.conn.close()
      
      self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

      try:
        self.conn.connect((self.ip, self.port))
      except ConnectionRefusedError as e:
        self.conn.close()

        raise e
      else:
        break

  def close(self):
    self.conn.close()

  def try_send_msg(self, msg):
    while True:
      try:
        self.send_msg(msg, client=True)
      except (BrokenPipeError, ConnectionResetError, ConnectionRefusedError, OSError):
        try:
          self.reconnect()
        except ConnectionRefusedError as e:
          raise e
        else:
          raise RuntimeError
      else:
        break

  def try_recv_msg(self, sock=None, block=True):
    while True:
      try:
        data = self.recv_msg(block=False)
      except BlockingIOError:
        continue
      except (BrokenPipeError, ConnectionResetError, OSError):
        try:
          self.reconnect()
        except ConnectionRefusedError as e:
          raise e
        else:
          raise RuntimeError
      else:
        break

    return data