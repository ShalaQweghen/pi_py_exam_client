from tkinter import *
from tkinter.messagebox import showwarning

from threading import Event, Thread

from lib.gui.question_page import QuestionPage

class WelcomePage(Frame):
  def __init__(self, parent, student_number, std_info, skip_pick=False):
    self.parent = parent

    self.name = std_info[0]
    self.exam_duration = std_info[1][0]
    self.questions = std_info[1][1]
    self.dersler = std_info[1][2]

    self.std_info = std_info
    
    self.td_questions = self.questions.get(9901203, None)
    self.yd_questions = self.questions.get(9901202, None)
    self.aiit_questions = self.questions.get(9901201, None)

    self.student_number = student_number

    self.lesson_var = StringVar()
  
    self.start_event = Event()
    self.wait_thread = Thread(target=self.wait_start, args=(self.start_event,))

    self.conn = self.parent.master.conn

    Frame.__init__(self, parent, bg='azure')
    self.grid(row=0, column=0, sticky=S+N+E+W)

    self.parent.master.bind("<Return>", lambda e: None)

    if skip_pick:
      self.wait_start_exam()
    else:
      self.std_info = None
      
      self.draw()

  def draw(self):
    Label(self, text='Hoşgeldin, ' + self.name.lower().title(), font=('times', 45, 'bold'), bg='azure', pady=90).pack(side=TOP)

    self.first_frame = Frame(self, bg="azure")
    self.first_frame.pack()

    Label(self.first_frame, text='Başlamadan önce lütfen okuyun:', font=('times', 25, 'bold'), bg='azure').pack(side=TOP)
    info = Text(self.first_frame, bg="azure", width=100, padx=10, pady=10, height=5, font=('times', 16))
    info.insert(END, '- Burada sınav hakkında bilgiler olacak')
    info.config(state=DISABLED)
    info.pack(side=TOP, pady=20)

    rs = []

    for i in self.dersler:
      if i == 9901203:
        message = "Türk Dili"
      elif i == 9901202:
        message = "Yabancı Dil"
      elif i == 9901201:
        message = "Ataturk İlkeleri ve İnkılap Tarihi"

      r = Radiobutton(self.first_frame, bg='azure', text=message, variable=self.lesson_var, value=i, font=('times', 24), wraplength=800)
      r.pack(side=TOP, padx=30, pady=2)

      rs.append(r)

    rs[0].select()
    self.lesson_var.set(self.dersler[0])

    f = Frame(self.first_frame, bg="azure")
    f.pack(side=TOP, pady=30)
    Button(f, text='Sınava Başla', height=2, width=20, command=self.load_questions, font=("times", 18, "bold")).pack(side=LEFT, padx=30)
    # Button(f, text='Oturumu Kapat', bg='red', height=2, width=20, command=self.close_session, font=("times", 18, "bold")).pack(side=LEFT, padx=30)

  def fetch_questions(self):
    try:
      self.conn.try_send_msg([2, self.lesson_var.get(), self.student_number])
    except ConnectionRefusedError:
      showwarning('Sunucu Hatası', 'Sunucuya ulaşılamıyor. Salon başkanı ile görüşün.')
    except RuntimeError:
      self.load_questions(True)

    try:
      questions = self.conn.try_recv_msg(block=False)
    except ConnectionRefusedError:
      showwarning('Sunucu Hatası', 'Sunucuya ulaşılamıyor. Salon başkanı ile görüşün.')
    except RuntimeError:
      self.load_questions(True)
    else:
      return questions

  def wait_start(self, start_event):
    # 333 is the message sent from the server to check if a client is connected
    start = 333
    passing = False

    # Ignore 333 message
    while start == 333:
      try:
        start = self.conn.try_recv_msg(block=False)
        passing = True
      except ConnectionRefusedError:
        showwarning('Sunucu Hatası', 'Sunucuya ulaşılamıyor. Salon başkanı ile görüşün.')
      except RuntimeError as e:
        self.wait_start(start_event)
    
    if passing:
      if start == 666:
        start_event.set()

  def check_start(self):
    if not self.start_event.is_set():
      self.parent.master.after(1000, self.check_start)
    else:
      self.start_exam()

  def wait_start_exam(self, again=False):
    if not again:
      if not self.std_info:
        self.first_frame.destroy()

      Label(self, text="*** LÜTFEN SALON BAŞKANININ SINAVI BAŞLATMASINI BEKLEYİNİZ ***", font=('times', 20, 'bold')).pack(side=TOP, pady=40)

      self.wait_thread.start()

    self.check_start()

  def start_exam(self):
    if self.std_info:
      QuestionPage(self.parent, self.exam_duration, self.questions, self.dersler, self.student_number)
    # else:
    #   QuestionPage(self.parent, self.questions, [self.student_number, self.ip, self.lesson_var.get()])

    self.destroy()

  def load_questions(self, again=False):
    self.questions = self.fetch_questions()

    self.wait_start_exam(again)
    
  def close_session(self):
    try:
      self.conn.try_send_msg([9, self.student_number])
    except (BrokenPipeError, ConnectionResetError, ConnectionRefusedError):
      pass

    self.conn.close()

    self.destroy()