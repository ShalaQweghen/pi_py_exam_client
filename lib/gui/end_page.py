from tkinter import *
# from lib.gui.entry_page import EntryPage

class EndPage(Frame):
  def __init__(self, parent, ogr_no, grades):
    self.parent = parent
    self.ogr_no = ogr_no

    self.grades = grades

    self.grades = grades

    self.conn = self.parent.master.conn

    Frame.__init__(self, parent, bg='azure')
    self.grid(row=0, column=0, sticky=S+N+E+W)

    self.parent.master.bind("<Left>", lambda e: None)
    self.parent.master.bind("<Right>", lambda e: None)
    self.parent.master.bind("<Up>", lambda e: None)
    self.parent.master.bind("<Down>", lambda e: None)
    self.parent.master.bind("<Key>", lambda e: None)

    self.parent.master.bind("<Return>", self.close_session)

    self.draw()

  def draw(self):
    Label(self, text='SINAV BİTMİŞTİR!', font=('times', 45, 'bold'), bg='azure', pady=90).pack(side=TOP)
    Label(self, text='SINAV NOTLARINIZ', font=('times', 25, 'bold'), bg='azure').pack(side=TOP)
    info = Text(self, bg="azure", width=100, padx=10, pady=10, font=('times', 16))

    height = 2
    
    for k in self.grades.keys():
      grades = self.grades[k][0]
      name = self.grades[k][1]

      if grades[0] != None:
        info.insert(END, "{} Sınav Notu: {}\n".format(name, grades[0]))

      if grades[1] != None:
        info.insert(END, "{} Kısa Sınav Notu: {}\n".format(name, grades[1]))

      info.insert(END, "\n")

      height += 2

    info.config(state=DISABLED, height=height)
    info.pack(side=TOP)

    f = Frame(self, bg="azure")
    f.pack(side=TOP, pady=60)

    Button(f, text='Oturumu Kapat', bg="red", height=2, width=20, command=self.close_session, font=("times", 18, "bold")).pack(side=LEFT, padx=30)

  def close_session(self, event=None):
    try:
      self.conn.try_send_msg([9, self.ogr_no])
    except (BrokenPipeError, ConnectionResetError, ConnectionRefusedError):
      pass

    self.conn.close()

    # EntryPage(self.parent)
    
    self.parent.master.bind("<Return>", self.parent.entry_page.enter)

    self.destroy()