from tkinter import *
from tkinter.messagebox import showwarning

import base64, time

from io import BytesIO

from lib.client import Client
from lib.gui.welcome_page import WelcomePage
from lib.gui.question_page import QuestionPage

class EntryPage(Frame):
  def __init__(self, parent):
    self.parent = parent

    self.std_num_var = StringVar()
    self.id_no_var = StringVar()

    self.cam = self.parent.master.cam

    self.parent.master.bind("<Return>", self.enter)

    self.parent.entry_page = self # This is to be able to bind <Return> again in EndPage

    Frame.__init__(self, parent, bg="azure")
    self.grid(row=0, column=0, sticky=S+N+E+W)

    self.draw()

  def draw(self):
    Label(self, text='HARUZEM SINAV SİSTEMİ', font=('times', 45, 'bold'), bg='azure', pady=90).pack(side=TOP)
    self.frame = Frame(self, bg="azure")
    self.frame.pack(side=TOP)

    f1 = Frame(self.frame, bg="azure")
    f1.pack(side=TOP)

    Label(f1, bg="azure", text="TC Kimlik No:", font=('times', 20, 'bold')).pack(side=LEFT, pady=15)
    e1 = Entry(f1, textvariable=self.id_no_var, font=('times', 16, 'bold'))
    e1.pack(side=LEFT)
    e1.focus_set()

    f2 = Frame(self.frame, bg="azure")
    f2.pack(side=TOP)

    Label(f2, bg="azure", text="Öğrenci No:", font=('times', 20, 'bold')).pack(side=LEFT, pady=15)
    e2 = Entry(f2, textvariable=self.std_num_var, font=('times', 16, 'bold'))
    e2.pack(side=LEFT)

    Button(self.frame, text="Giriş Yap", command=self.enter, font=("times", 18, "bold"), height=2, width=20).pack(side=TOP, pady=20)
    Button(self.frame, text="Çıkış", bg="red", fg="white", command=self.parent.master.quit_with_password).pack(side=BOTTOM, pady=10)

  def start_session(self):
    try:
      self.conn = Client("", 33332)
    except ConnectionRefusedError:
      showwarning('Sunucu Hatası', 'Sunucuya ulaşılamıyor. Salon başkanı ile görüşün.')
    else:
      self.parent.master.conn = self.conn

  def enter(self, connect=True):
    if connect:
      self.start_session()

    try:
      self.conn.try_send_msg([1, self.std_num_var.get(), self.id_no_var.get()])
    except ConnectionRefusedError:
      showwarning('Sunucu Hatası', 'Sunucuya ulaşılamıyor. Salon başkanı ile görüşün.')
    except RuntimeError:
      self.enter(False)
    except AttributeError:
      pass
    else:
      # 333 is sent from the server to check if a client is still connected
      std_info = 333

      # Ignore 333 message
      while std_info == 333:
        try:
          std_info = self.conn.try_recv_msg(block=False)
          passing = True
        except ConnectionRefusedError:
          passing = False

          showwarning('Sunucu Hatası', 'Sunucuya ulaşılamıyor. Salon başkanı ile görüşün.')
        except RuntimeError:
          passing = False

          self.enter(False)
      
      if passing:
        if std_info:
          if std_info == 888:
            showwarning("SINAV UYARISI", "Öğrenci numaranızı kontrol edin ve tekrar deneyin.")

            try:
              self.conn.try_send_msg([9, self.std_num_var.get()])
            except (BrokenPipeError, ConnectionResetError, ConnectionRefusedError):
              pass

            self.conn.close()

            self.std_num_var.set("")
            self.id_no_var.set('')
          elif std_info == 777:
            showwarning("SINAV UYARISI", "Başlamış bir sınava katılamazsınız.")

            try:
              self.conn.try_send_msg([9, self.std_num_var.get(), self.id_no_var.get()])
            except (BrokenPipeError, ConnectionResetError, ConnectionRefusedError):
              pass

            self.conn.close()

            self.std_num_var.set("")
            self.id_no_var.set('')
          elif len(std_info) == 2: 
            # Starting a new exam
            # std_info = [student name, [exam duration, questions, lessons]]
            WelcomePage(self.parent, self.std_num_var.get(), std_info, True)

            self.std_num_var.set('')
            self.id_no_var.set('')
          elif len(std_info) == 3: 
            # Continuing an unfinished exam 
            # std_info = [student name, [None, questions, lessons], remaining time]
            QuestionPage(self.parent, std_info[1][0], std_info[1][1], std_info[1][2], self.std_num_var.get(), std_info[2])

            self.std_num_var.set('')
            self.id_no_var.set('')
          else:
            if self.cam:
              self.frame.destroy()
              canvas = Canvas(self, width=600, height=338)
              canvas.pack(side=TOP)
              
              stream = self.take_picture(canvas)
              
              f = Frame(self, bg='azure')
              f.pack(side=TOP)
              
              Button(f, text='Resmi Onayla', bg="green", height=2, padx=5, command=lambda:self.continue_to_welcome(stream, std_info), font=("times", 18)).pack(side=LEFT, pady=40, padx=50)      
              Button(f, text='Tekrar Çek', bg="blue", height=2, padx=5, command=lambda: self.take_picture(canvas), font=("times", 18)).pack(side=LEFT, pady=40, padx=50)
            else:
              self.continue_to_welcome(None, std_info)
        else:
          showwarning("Sınav Hatası", "Bu oturumda aktif sınavınız bulunmamaktadır.")

          try:
            self.conn.try_send_msg([9, self.std_num_var.get()])
          except (BrokenPipeError, ConnectionResetError, ConnectionRefusedError):
            pass
          
          self.std_num_var.set("")
          self.id_no_var.set('')

          self.conn.close()
  
  def take_picture(self, canvas):
    showwarning("Kamera Uyarısı", "Kamera başlatılıyor. Hazır olduğunuzda lütfen tamama basın.")
    
    stream = BytesIO()
    
    self.cam.start_preview()
    time.sleep(3)
    self.cam.capture(stream, format='gif', resize=(600, 338))
    self.cam.stop_preview()
    
    stream.seek(0)
    img_data = base64.b64encode(stream.read())
    self.img = PhotoImage(data=img_data)
    canvas.create_image(0, 0, anchor=NW, image=self.img)
    stream.seek(0)

    return stream

  def continue_to_welcome(self, stream, std_info):
    name = '{}--{}.gif'.format(self.std_num_var.get(), datetime.datetime.now())

    if stream:
      stream.seek(0)
      
    # send the picture to server
    try:
      self.conn.try_send_msg([3, stream and stream.read(), name])
    except ConnectionRefusedError:
      showwarning('Sunucu Hatası', 'Sunucuya ulaşılamıyor. Salon başkanı ile görüşün.')
    except RuntimeError:
      self.continue_to_welcome(stream, std_info)
    else:
      student_number = self.std_num_var.get()

      self.parent.master.student_number = student_number
 
      WelcomePage(self.parent, student_number, std_info)