from tkinter import Frame, Text, Scrollbar
from tkinter.constants import YES, BOTH, RIGHT, Y

class PiText(Frame):
    def __init__(self, master, width=0, height=0, **kwargs):
        self.__width = width
        self.__height = height

        Frame.__init__(self, master, width=self.__width, height=self.__height)

        self.__scroll = Scrollbar(self)

        self.text = Text(self, yscrollcommand=self.__scroll.set, **kwargs)

        self.__scroll.config(command=self.text.yview)

        self.text.pack(expand=YES, fill=BOTH)

    def pack(self, *args, **kwargs):
        Frame.pack(self, *args, **kwargs)
        self.pack_propagate(False)

    def grid(self, *args, **kwargs):
        Frame.grid(self, *args, **kwargs)
        self.grid_propagate(False)

    def scroll_pack(self):
        self.text.pack_forget()

        self.__scroll.pack(side=RIGHT, fill=Y)

        self.text.pack(expand=YES, fill=BOTH)

    def scroll_unpack(self):
        self.__scroll.pack_forget()