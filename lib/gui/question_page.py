from tkinter import *
from tkinter.messagebox import showwarning, askyesno
from tkinter.font import Font

import datetime, os, random, math, sys

from collections import OrderedDict

from lib.gui.end_page import EndPage
from lib.gui.pi_text import PiText

if os.sys.platform in ["linux", "darwin"]:
  SAVE_DIR = os.path.join(os.environ["HOME"], "HARUZEM_CLIENT")
else:
  SAVE_DIR = os.path.join(os.environ["HOMEPATH"], "HARUZEM_CLIENT")

class QuestionPage(Frame):
  def __init__(self, parent, exam_duration, questions, dersler, student_number, rem_time=None):
    self.parent = parent
    self.student_number = student_number

    self.txt_name = "{}-{}.txt".format(self.student_number, datetime.date.today())

    self.opt_idx = 0
    self.ders_idx = 0
    self.all_questions = questions

    random.shuffle(dersler)

    self.dersler = [d[0] for d in dersler]
    self.isimler = [d[1] for d in dersler]

    self.questions = self.all_questions[self.dersler[self.ders_idx]][0]

    self.p_var = StringVar()
    self.a_var = StringVar()
    self.b_var = StringVar()
    self.c_var = StringVar()
    self.d_var = StringVar()
    self.e_var = StringVar()
    self.opt_var = StringVar()
    self.ders_ad = StringVar()
    self.q_no_var = StringVar()
    self.time_info = StringVar()

    self.font = Font(family="times", size=-25)

    self.conn = self.parent.master.conn

    self.answers = dict()
    self.prev_answers = dict()

    for d in self.dersler:
      self.answers[d] = OrderedDict()
      self.prev_answers[d] = list(self.all_questions[d][1])

    for k in self.prev_answers.keys():
      for i, a in enumerate(self.prev_answers[k]):
        self.answers[k][i + 1] = a

    saved_ans_file = os.path.join(SAVE_DIR, "fragment-{}".format(self.txt_name))
    
    minutes = None
    seconds = None
    
    if os.path.exists(saved_ans_file):
      saved_ans_file = open(saved_ans_file, "r")
      saved_ans = saved_ans_file.read().split("\n")

      disconnected = False

      for answer in saved_ans:
        if answer:
          module, rem_info = answer.split("=")
          q_no, rem_info = rem_info.split(":")
          option, rem_info = rem_info.split("+")
          minutes, seconds = rem_info.split("-")

          self.answers[int(module)][int(q_no)] = option

          try:
            self.conn.try_send_msg([4, int(module), int(q_no), option, (int(minutes), int(seconds))])
          except ConnectionRefusedError:
            disconnected = True

            pass

      saved_ans_file.close()

      if not disconnected:
        os.remove(saved_ans_file)
      
    try:
      idx = self.prev_answers[self.dersler[self.ders_idx]].index("Y")
    except ValueError as e:
      idx = 0

    self.pq_num = 0
    self.q_idx = idx - 1
    self.q_no = idx
    self.options = []
    self.tl = None

    if rem_time:
      self.minutes = rem_time // 60
      self.seconds = rem_time % 60
    elif minutes and seconds:
      self.minutes = int(minutes)
      self.seconds = int(seconds)
    else:
      self.minutes = exam_duration // 60
      self.seconds = exam_duration % 60

    self.exam_ended = False
    self.parag_to_show = True

    self.start_date = datetime.date.today()
    self.start_time = datetime.datetime.now().time()

    if not os.path.exists(SAVE_DIR):
      os.mkdir(SAVE_DIR)

    Frame.__init__(self, parent, bg='azure')

    self.grid(row=0, column=0, sticky=S+N+E+W)

    self.parent.master.bind("<Key>", self.select_by)

    self.parent.master.bind("<Left>", self.prev_question)
    self.parent.master.bind("<Right>", self.next_question)
    self.parent.master.bind("<Up>", self.select_up)
    self.parent.master.bind("<Down>", self.select_down)

    self.draw()

  def draw(self):
    Label(self, textvariable=self.time_info, font=("times", -25, 'italic bold'), bg="#004d00", fg="azure").pack(side=TOP, pady=30)
    Label(self, textvariable=self.ders_ad, font=("times", -25, 'bold'), bg='azure').pack(side=TOP)
    Label(self, textvariable=self.q_no_var, font=('times', -40, 'italic'), bg='azure').pack(side=TOP)

    f1 = Frame(self, bg="azure", width=900)
    
    # Custom Text widget class that resizes according to the text
    self.question = PiText(f1, font=('times', -25), wrap=WORD, bg='azure', bd=0, insertwidth=0, padx=0, highlightthickness=0)

    self.question.pack(side=TOP, pady=15)

    f1.pack()

    self.p_button = Button(f1, text='Metni Göster', bg='green', height=2, padx=5, command=self.show_paragraph)

    self.f3 = Frame(self, bg="azure", width=900)
    self.f3.pack()

    for option, s, v in zip(self.questions[self.q_idx][1:], ["A", "B", "C", "D", "E"], [self.a_var, self.b_var, self.c_var, self.d_var, self.e_var]):
      r = Radiobutton(self.f3, bg='azure', textvariable=v, variable=self.opt_var, value=s, command=self.record_answer, font=('times', -24), wraplength=900, justify=LEFT)
      r.pack(anchor=NW, padx=30, pady=2)
      self.options.append(r)

    f2 = Frame(self, bg="azure", width=900)
    f2.pack()

    self.prev_button = Button(f2, text='Önceki Soru', bg="green", height=2, padx=5, command=self.prev_question, font=("times", -18))
    self.prev_button.config(state=DISABLED)
    self.prev_button.pack(side=LEFT, pady=40, padx=50)

    self.finish_button = Button(f2, text="Sinavi Bitir", bg="azure", disabledforeground="azure", height=2, padx=5, command=self.end_exam, font=("times", -18))
    self.finish_button.config(state=DISABLED)
    self.finish_button.pack(side=LEFT, pady=40, padx=50)

    self.next_button = Button(f2, text='Sonraki Soru', bg="green", height=2, padx=5, command=self.next_question, font=("times", -18))
    self.next_button.pack(side=RIGHT, pady=40, padx=50)

    self.next_question()
    self.countdown()

  def select_by(self, event):
    c = event.char.lower()

    if c in ['a', 'b', 'c', 'd', 'e']:
      if c == 'a':
        self.options[0].select()
      elif c == 'b':
        self.options[1].select()
      elif c == 'c':
        self.options[2].select()
      elif c == 'd':
        self.options[3].select()
      elif c == 'e':
        self.options[4].select()

      self.record_answer()

  def select_down(self, event):
    if self.opt_idx < 4:
      self.opt_idx += 1

      self.options[self.opt_idx].select()

      self.record_answer()

  def select_up(self, event):
    if self.opt_idx > 0:
      self.opt_idx -= 1

      self.options[self.opt_idx].select()

      self.record_answer()

  def next_question(self, showP=False):
    if self.q_no == len(self.questions):
        if self.ders_idx == len(self.dersler) - 1:
          self.finish_button.config(state=NORMAL, bg='red')
          self.next_button.config(state=DISABLED)
          self.parent.master.bind("<Return>", self.end_exam)
        else:
          self.ders_idx += 1
          self.questions = self.all_questions[self.dersler[self.ders_idx]][0]

          self.q_no = 0
          self.q_idx = -1

    if self.q_idx < len(self.questions) - 1:
      self.q_idx += 1

      if self.pq_num == 3:
        self.pq_num = 0
        self.parag_to_show = True

      if not self.parag_to_show:
        self.pq_num += 1

      if self.questions[self.q_idx][-1] != 0 and self.parag_to_show and self.pq_num == 0:
        self.pq_num += 1
        self.show_paragraph(True)

      self.q_no += 1
      self.q_no_var.set("SORU " + str(self.q_no))

      if self.q_no > 1:
        self.prev_button.config(state=NORMAL)

      self.load_question()

  def prev_question(self, event=None):
    if self.q_no == 1 and self.ders_idx != 0:
      self.ders_idx -= 1
      self.questions = self.all_questions[self.dersler[self.ders_idx]][0]
      self.q_idx = len(self.questions)
      self.q_no = self.q_idx + 1

    if self.q_idx > 0:
      self.q_idx -= 1

      self.q_no -= 1

      self.q_no_var.set("SORU " + str(self.q_no))
      
      if self.q_no == len(self.questions) - 1:
        self.next_button.config(state=NORMAL)

      self.load_question()

      if self.q_no == 1 and self.ders_idx == 0:
        self.prev_button.config(state=DISABLED)

  def record_answer(self, recording=True):
    answer = self.opt_var.get()

    self.answers[self.dersler[self.ders_idx]][self.q_no] = answer

    if recording:
      txt = open(os.path.join(SAVE_DIR, "fragment-{}".format(self.txt_name)), "a")

      txt.write("{}={}:{}+{}-{}\n".format(self.dersler[self.ders_idx], self.q_no, answer, self.minutes, self.seconds))
        
      txt.close()

    # In case client goes down before the exam ends
    try:
      self.conn.try_send_msg([4, self.dersler[self.ders_idx], self.q_no, answer, (self.minutes, self.seconds)])
    except ConnectionRefusedError:
      pass
    except RuntimeError:
      self.record_answer(False)

  def destroy_toplevel(self, toplevel):
    toplevel.destroy()
    self.tl = None

  def show_paragraph(self, show_header=False):
    if not self.tl:
      tl = Toplevel()
      tl.config(bg="azure")
      tl.protocol("WM_DELETE_WINDOW", lambda: self.destroy_toplevel(tl))
      tl.transient()

      if show_header:
        p_i = Message(tl, text="Sıradaki 3 soruyu metne göre cevaplayınız.", width=900, fg='red', font=('times', -25))
        p_i.pack(side=TOP, pady=15)

        self.parag_to_show = False

      info = Message(tl, textvariable=self.p_var, width=900, font=('times', -25))
      info.pack(side=TOP, pady=15)

      self.tl = tl
    else:
      self.tl.lift()

  def parse_question(self, question):
    def parse(rg, text, parsed_list=[]):
      s_start = re.search(rg, text)

      if s_start:
        s_start = s_start.span()

        s_end = re.search(rg, text[s_start[1]:]).span()

        if parsed_list:
          s_start = (parsed_list[-1][1] + s_start[0], parsed_list[-1][1] + s_start[1])

        s_span = [s_start[0], s_start[0] + s_end[0]]

        return parse(rg, text[s_end[1] + s_start[1]:], [*parsed_list, s_span])
      else:
        return parsed_list

    bold = parse(r"\*", question)
    question = question.replace("*", "")
    underline = []
    # underline = parse(r"_", question)
    #question = question.replace("_", "")
    italics = parse(r"#", question)
    question = question.replace("#", "")

    return [question.rstrip(), (bold, underline, italics)]

  def load_question(self, recording=True):
    self.f3.pack()
    self.p_button.pack_forget()

    self.ders_ad.set(self.isimler[self.ders_idx])

    parsed_question, (bold, underline, italics) = self.parse_question(self.questions[self.q_idx][2])

    lines = parsed_question.split("\n")

    width = 0
    height = 0

    for line in lines:
      width = max(width, self.font.measure(line))

      if width > 910:
        height += math.ceil(width / 910)
      else:
        height += 1

    # print(width, height)

    if width > 910:
      width = 910

    if height > 10:
      height = 10

      self.question.scroll_pack()
    else:
      self.question.scroll_unpack()

    height += 1
    height *= self.font.metrics('linespace')
    width += 10

    self.question.config(height=height, width=width)

    self.question.text.config(state=NORMAL)

    self.question.text.delete("1.0", END)
    
    self.question.text.insert(INSERT, parsed_question)

    for b in bold:
      self.question.text.tag_add("bold", "1." + str(b[0]), "1." + str(b[1]))
      self.question.text.tag_config("bold", font=("times", -25, "bold"))

    for u in underline:
      self.question.text.tag_add("underline", "1." + str(u[0]), "1." + str(u[1]))
      self.question.text.tag_config("underline", underline=1)

    for i in italics:
      self.question.text.tag_add("italics", "1." + str(i[0]), "1." + str(i[1]))
      self.question.text.tag_config("italics", font=("times", -25, "italic"))

    self.question.text.config(state=DISABLED)

    self.a_var.set("A) " + self.questions[self.q_idx][3])
    self.b_var.set("B) " + self.questions[self.q_idx][4])
    self.c_var.set("C) " + self.questions[self.q_idx][5])
    self.d_var.set("D) " + self.questions[self.q_idx][6])
    self.e_var.set("E) " + self.questions[self.q_idx][7])

    if self.questions[self.q_idx][-1] != 0:
      self.p_button.pack()
      self.p_var.set(self.questions[self.q_idx][1])

    ans = self.answers[self.dersler[self.ders_idx]].get(self.q_no)

    if ans == "Y":
      o = 0

      self.answers[self.dersler[self.ders_idx]][self.q_no] = "A"

      if recording:
        txt = open(os.path.join(SAVE_DIR, "fragment-{}".format(self.txt_name)), "a")

        txt.write("{}={}:{}+{}-{}\n".format(self.dersler[self.ders_idx], self.q_no, "A", self.minutes, self.seconds))
          
        txt.close()

      try:
        self.conn.try_send_msg([4, self.dersler[self.ders_idx], self.q_no, "A", (self.minutes, self.seconds)])
      except ConnectionRefusedError:
        pass
      except RuntimeError:
        self.load_question()
    else:
      if ans == "B":
        o = 1
      elif ans == "C":
        o = 2
      elif ans == "D":
        o = 3
      elif ans == "E":
        o = 4
      else:
        o = 0

    self.opt_idx = o

    self.options[o].select()

  def countdown(self):
    if self.seconds == 0:
      self.seconds = 59
      self.minutes -= 1
    else:
      self.seconds -= 1

    if self.seconds >= 0 and self.minutes >= 0:
      if self.seconds > 9:
        seconds = str(self.seconds)
      else:
        seconds = '0' + str(self.seconds)

      self.time_info.set('{}:{}'.format(self.minutes, seconds))
      self.parent.master.after(1000, self.countdown)
    elif not self.exam_ended:
      self.end_exam(True)

  def calc_grade(self, code):
    questions = self.all_questions.get(code, None)
    answers = self.answers.get(code, None)

    if not questions:
      return (None, None)

    grade = 0

    if len(questions[0]) > 20:
      k_grade = 0
    else:
      k_grade = None

    for i, q in enumerate(questions[0]):
      if answers[i + 1] == q[8]:
        if i < 21:
          grade += 5
        else:
          k_grade += 5

    return (grade, k_grade)

  def end_exam(self, time_up=False):
    if not time_up:
      decision = askyesno("TEYİT", "Sınavı bitirmek istediğinize emin misiniz?")
    else:
      decision = True
      

    if decision:
      grades = dict()

      for d, i in zip(self.dersler, self.isimler):
        grades[d] = [self.calc_grade(d), i]

      self.send_answers(grades)

      EndPage(self.parent, self.student_number, grades)

      self.destroy()

  def send_answers(self, grades, recording=True):
    if recording:
      txt = open(os.path.join(SAVE_DIR, self.txt_name), "w")

      for k in self.answers.keys():
        txt.write(str(k) + "=" + "".join(list(self.answers[k].values())) + "\n")
      
      txt.close()

      os.remove(os.path.join(SAVE_DIR, "fragment-{}".format(self.txt_name)))

    try:
      self.conn.try_send_msg([2, [self.answers, grades]])
    except ConnectionRefusedError:
      showwarning('Sunucu Hatası', 'Sunucuya ulaşılamıyor. Cevaplarınız kaydedildi. Salon başkanı ile görüşün.')
    except RuntimeError:
      self.send_answers(grades, False)