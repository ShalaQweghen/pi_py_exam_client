from tkinter import *

from lib.gui.entry_page import EntryPage

import hashlib

try:
  import picamera
except ModuleNotFoundError:
  picamera = None

class Root(Tk):
  def __init__(self):
    Tk.__init__(self)
    self.geometry("{}x{}+0+0".format(self.winfo_screenwidth(), self.winfo_screenheight()))
    self.resizable(False, False)
    self.protocol('WM_DELETE_WINDOW', self.quit_with_password)
    self.bind("<Unmap>", self.unmap)
    # self.wm_attributes('-type', 'splash')
    # self.overrideredirect(True)

    self.title("HARUZEM SINAV SİSTEMİ")
    self.config(bg="azure")

    self.p_var = StringVar()

    if picamera:
      try:
        self.cam = picamera.PiCamera()
      except (picamera.exc.PiCameraMMALError, picamera.exc.PiCameraError):
        self.cam = None
    else:
      self.cam = None

    self.hash = "8e510a081b1f75cf437fdc9725de67a5"
    self.conn = None
    self.student_number = None

    self.container = Frame(self, bg='azure')
    self.container.pack(side=TOP, fill=BOTH, expand=YES)
    self.container.grid_rowconfigure(0, weight=1)
    self.container.grid_columnconfigure(0, weight=1)

    EntryPage(self.container)

  def unmap(self, event):
    if event.widget is self:
      self.deiconify()

  def quit_program(self):
    self.destroy()

  def quit_with_password(self):
    tl = Toplevel()

    ws = self.winfo_screenwidth()
    x = int((ws/2) - (704/2))

    tl.geometry('+{}+{}'.format(x, self.winfo_height() // 2))

    Label(tl, text="Şifre: ").grid(row=0, column=0)

    e = Entry(tl, textvariable=self.p_var, show="*")
    e.grid(row=0, column=1)
    e.focus_set()

    Button(tl, text="Gönder", command=lambda: self.check_password(tl)).grid(row=1, column=0, columnspan=2)

  def check_password(self, tl):
    p = self.p_var.get()

    if hashlib.md5(p.encode()).hexdigest() == self.hash:
      tl.destroy()

      self.quit_program()
    else:
      self.p_var.set("")