import socket, struct, pickle

class Connection():
  def __init__(self):
    self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

  def recv_msg(self, sock=None, block=True):
    if not sock:
      sock = self.conn
    # First 4 bytes are the length of the message
    # so that the message can be captured correctly
    try:
      raw_msglen = self.recvall(4, sock, False, block)
    except (ConnectionResetError, BrokenPipeError) as e:
      raise e

    msglen = struct.unpack('>I', raw_msglen)[0]

    return self.recvall(msglen, sock)

  def recvall(self, n, sock=None, pickled=True, block=True):
    if not sock:
      sock = self.conn

    # To be able to allow quitting when it is not the player's
    # turn, the connection should be non-blocking
    sock.setblocking(block)

    data = b''

    while len(data) < n:
      # Raises BlockingIOError if there is no data available
      # if the blocking is set to False
      try:
        packet = sock.recv(n - len(data))
      except (ConnectionResetError, BrokenPipeError) as e:
        raise e

      # If there is data, turn the connection into blocking
      # in order to avoid EOFError from pickle
      sock.setblocking(True)

      data += packet

    if pickled:
      return pickle.loads(data)
    else:
      return data

  def send_msg(self, msg, sock=None, client=False):
    if not sock:
      sock = self.conn

    pickled = pickle.dumps(msg)

    # The length of the message is added to the beginning
    # so that all the message can be captured correctly
    pickled = struct.pack('>I', len(pickled)) + pickled

    sock.sendall(pickled)